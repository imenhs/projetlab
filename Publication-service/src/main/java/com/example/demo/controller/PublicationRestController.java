package com.example.demo.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParseException;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.dao.PublicationRepository;
import com.example.demo.entity.Publication;
import com.example.demo.services.PublicationService;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;
import java.util.Optional;

import javax.servlet.ServletContext;



@RestController

public class PublicationRestController {
	@Autowired
	PublicationRepository publicationRepository;
	@Autowired  
	ServletContext context;
	@Autowired 
	PublicationService publicationService;
	@GetMapping("/publications")
	  public ResponseEntity<List<Publication>> getAllPublications(){
	      List <Publication>publications= new ArrayList<Publication>();

	      try {
	    	  publicationRepository.findAll().forEach(publications::add);
	          
	          if (publications.isEmpty()) {
	            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	          }
	          return new ResponseEntity<>(publications, HttpStatus.OK);
	        } catch (Exception e) {
	          return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	        }
	  }
	 @PutMapping("/publications/{id}")
	  public ResponseEntity<Publication> updatePublication(@PathVariable("id") long id, @RequestBody Publication publication) {
	    Optional<Publication> PublicationData =publicationRepository.findById(id);
	 
	    if (PublicationData.isPresent()) {
	    	Publication _publication =PublicationData.get();
	    	_publication.setTitre(publication.getTitre());
	    	_publication.setType(publication.getType());
	    	_publication.setDateApparition(publication.getDateApparition());
	    	_publication.setLien(publication.getLien());
	    	_publication.setSourcePdf(publication.getSourcePdf());
	      return new ResponseEntity<>(publicationRepository.save(_publication), HttpStatus.OK);
	    } else {
	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    }
	  }

	
		@GetMapping( value = "/publications/{id}")
		public Publication recupererUnePublication(@PathVariable("id") Long id) {
			
			return publicationRepository.findById(id).get(); 
			
		}
		 @DeleteMapping("/publications/{id}")
		  public void deletePublication(@PathVariable("id") long id) {
		    Optional<Publication> PublicationData =publicationRepository.findById(id);
		 
		    if (PublicationData.isPresent()) {  publicationRepository.deleteById(id);
		    }} 
		 
		 @PostMapping("/publications")
		 public Publication createPublication (@RequestParam("pubSource") MultipartFile pubSource,
				 @RequestParam("publication") String publication) throws JsonParseException , JsonMappingException , Exception
		 {	System.out.println(publication);
		 
			 System.out.println("Ok .............");
			 Publication pub = new ObjectMapper().readValue(publication, Publication.class);
			 pub.setDateApparition(new Date());
		
	        boolean isExitPdf = new File(context.getRealPath("/webapp/Publications/")).exists();
	        
	      
	        if (!isExitPdf)
	        {
	        
	        	new File (context.getRealPath("/Publications/")).mkdir();
	        	System.out.println("mkdir Publications.........");
	        }
	        String Publicationname = pubSource.getOriginalFilename();
	        String newPublicationName = FilenameUtils.getBaseName(Publicationname)+"."+FilenameUtils.getExtension(Publicationname);
	       
	        File serverPubSource = new File (context.getRealPath("/Publications/"+File.separator+newPublicationName));
	
	        try
	        {
	        	System.out.println("Publication");
	        	 FileUtils.writeByteArrayToFile(serverPubSource,pubSource.getBytes());

	        	 
	        }catch(Exception e) {
	        	e.printStackTrace();
	        }

	        pub.setSourcePdf(newPublicationName);
	        
	        return publicationRepository.save(pub);
	        
		 }
		 @GetMapping("/files/{filename}")
		  @ResponseBody
		  public ResponseEntity<Resource> getFile(@PathVariable String filename) {
		    Resource file = publicationService.load(filename);
		    return ResponseEntity.ok()
		        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
		  }
}
