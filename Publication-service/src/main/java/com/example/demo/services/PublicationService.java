package com.example.demo.services;

import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
@Service
public class PublicationService {
	String curDir = System.getProperty("user.dir");
	   
    private final Path root = Paths.get( curDir+"/src/main/webapp/Publications/");

	public Resource load(String filename) {
		 try {
		      Path file = root.resolve(filename);
		      Resource resource = new UrlResource(file.toUri());
		  	   if (resource.exists() || resource.isReadable()) {
		        return resource;
		      } else {
		        throw new RuntimeException("Could not read the file!");
		      }
		    } catch (MalformedURLException e) {
		      throw new RuntimeException("Error: " + e.getMessage());
		    } 

}
}