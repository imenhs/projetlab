package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.OutilRepository;
import com.example.demo.entities.Outil;
@Service
public class OutilImpl implements IOutilService {

	@Autowired
	OutilRepository outilRepository;
	@Override
	public Outil addOutil(Outil m) {
		outilRepository.save(m);
		return m;
	}

	@Override
	public void deleteOutil(Long id) {
		outilRepository.deleteById(id);

	}

	@Override
	public Outil updateOutil(Outil p) {
		return outilRepository.saveAndFlush(p);
		
	}

	@Override
	public Outil findOutil(Long id) {
		Outil o=outilRepository.findById(id).get();
		
		return o; 
	}

	@Override
	public List<Outil> findAll() {
		
		return outilRepository.findAll();
	}

}
