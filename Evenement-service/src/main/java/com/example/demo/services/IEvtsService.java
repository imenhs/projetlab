package com.example.demo.services;

import java.util.List;

import com.example.demo.entities.Evenement;


public interface IEvtsService {
	public Evenement addEvt(Evenement e);
	public void deleteEvt(Long id) ;
	public Evenement updateEvt(Evenement e) ;
	public Evenement findEvt(Long id) ;
	public List<Evenement> findAll();


}
