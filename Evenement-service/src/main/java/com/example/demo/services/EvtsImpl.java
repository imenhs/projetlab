package com.example.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.EvenementRepository;
import com.example.demo.entities.Evenement;


@Service
public class EvtsImpl implements IEvtsService {
	@Autowired
	EvenementRepository evtsRep;


	@Override
	public Evenement addEvt(Evenement e) {
		evtsRep.save(e);
		return e;	}

	@Override
	public void deleteEvt(Long id) {
		evtsRep.deleteById(id);
		
	}

	@Override
	public Evenement updateEvt(Evenement e) {
		return evtsRep.saveAndFlush(e);
	}

	@Override
	public Evenement findEvt(Long id) {
		return evtsRep.findById(id).orElse(null);

	}

	@Override
	public List<Evenement> findAll() {
		
		return evtsRep.findAll();
	}
}