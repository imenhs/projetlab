package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Evenement;
import com.example.demo.services.IEvtsService;



@RestController
public class EvenementRestController {

	@Autowired
	IEvtsService evtsService;
	
	@RequestMapping(value="/evenements", method=RequestMethod.GET)
	public List<Evenement> findEvts ()
	{
		return evtsService.findAll();
	}
	
	@GetMapping(value="/evenement/{id}")
	public Evenement findOneEvtById(@PathVariable Long id	)
	{
		return evtsService.findEvt(id);
	}
	
	@PostMapping(value="/evenement")
	public Evenement addEvt(@RequestBody Evenement e) {
		return evtsService.addEvt(e);
	}
	
	@DeleteMapping(value="/evenement/{id}")
	public void deleteEvt(@PathVariable Long id)
	{
		evtsService.deleteEvt(id);
	}
	
	@PutMapping(value="/evenement/{id}")
	public Evenement updateEvt(@PathVariable Long id, @RequestBody Evenement e) {
		e.setId(id);
		return evtsService.updateEvt(e);
	}
}

