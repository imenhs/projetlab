import { Component, OnInit } from '@angular/core';
import {Publication} from '../../models/publications.model';
import {PublicationsService} from '../../services/publications.service';
import {MatDialog} from '@angular/material';
import {DialogComponent} from '../../material-component/dialog/dialog.component';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import { AuthService } from 'app/services/auth.service';


@Component({
  selector: 'app-list-pub',
  templateUrl: './list-pub.component.html',
  styleUrls: ['./list-pub.component.css']
})
export class ListPubComponent implements OnInit {
  publicationData: Publication[] = [];
  protected _onDestroy = new Subject<void>();
  searchText:any;
  downloadPath=this.publicationService.path + '/files/';
  constructor(private publicationService : PublicationsService,
      private dialog: MatDialog,
      private authService: AuthService,) {

  }

  ngOnInit(): void {
    this.fetchDataSource();
  }

  private  fetchDataSource  (): void{
    this.publicationService.getAllPublications().then(data => this.publicationData = data);
  }
  ngOnDestroy(): void {
    this._onDestroy.next();
    this._onDestroy.complete();
  }
  onRemoveAccount(id: string) : void {
    const dialogRef = this.dialog.open(DialogComponent, {
      hasBackdrop: true,
      disableClose: false,
    });

    dialogRef.componentInstance.confirmButtonColor = 'warn';

    dialogRef.afterClosed().pipe(takeUntil(this._onDestroy)).subscribe(isDeleteConfirmed => {
      console.log('removing: ', isDeleteConfirmed);
      if (isDeleteConfirmed) {
        this.publicationService.removePublicationById(id).then(() => this.fetchDataSource());
      }
    }); 
   }

   isAdmin():boolean{
     
    return this.authService.isAdmin();
  }

}
