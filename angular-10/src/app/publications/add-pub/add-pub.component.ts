import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Publication} from '../../models/publications.model';
import {ActivatedRoute, Router} from '@angular/router';
import {PublicationsService} from '../../services/publications.service';
import { MembreService } from 'app/services/membre.service';

@Component({
  selector: 'app-add-pub',
  templateUrl: './add-pub.component.html',
  styleUrls: ['./add-pub.component.css']
})
export class AddPubComponent implements OnInit {
  form!: FormGroup;
  currentItemId!: string;
  item!: Publication;
  userSourcePdf: any;
  pdfURL: any;
  public pdfPath! : string;
  public message: string | undefined;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private membreService: MembreService,
    private publicationService: PublicationsService) {
  }

  ngOnInit(): void {
    this.currentItemId = this.activatedRoute.snapshot.params.id;
    console.log(this.currentItemId);
    if (!!this.currentItemId) {
      this.publicationService.getPublicationById(this.currentItemId).then(item => {
        this.item = item;
        this.initForm(item);
      });
    } else {
      this.initForm(null);
    }
  }

  initForm(item: any): void {
    this.form = new FormGroup({
        titre: new FormControl(item?.titre, [Validators.required]),
        type: new FormControl(item?.type, [Validators.required]),
        lien: new FormControl(item?.lien, [Validators.required]),
        dateApparition : new FormControl(new Date()),
        sourcePdf: new FormControl(item?.sourcePdf, [Validators.required]),
      }
    );

  }

  isFormInEditMode(): boolean {
    return !!this.currentItemId;
  }

  onSubmit(): void {
    const formData = new  FormData();
    const publication = this.form!.value;
    formData.append('publication',JSON.stringify(publication));
    formData.append('pubSource',this.userSourcePdf);
    if (this.isFormInEditMode()) {
      this.publicationService.updatePublication(this.currentItemId!,publication).then(() => {
        this.previousState();
      });
    } else {
      this.publicationService.createPublication(formData).then(data => {
        this.membreService.getCurrentUser().then(user =>{
          this.membreService.affecterPubMembre(user.id,data.id).then(
            data => {
              this.previousState()})

        })
      });
    }
  }

  previousState(): void {
    window.history.back();
  }
  onSelectSourcePdf(event: any) {
    if (event.target.files.length > 0)
    { const pdf = event.target.files[0];
      this.userSourcePdf = pdf;
      // this.f['profile'].setValue(file);
      var mimeType = event.target.files[0].type;
      if (mimeType.match(/pdf\/*/) == null) {
        this.message = "Only images are supported.";
        return;
      }
      var reader = new FileReader();
      this.pdfPath = pdf;
      reader.readAsDataURL(pdf);
      reader.onload = (_event) => {
        this.pdfURL = reader.result;
      }
    }
  }

  
}
