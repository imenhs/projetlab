import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddPubComponent } from './add-pub/add-pub.component';
import {ListPubComponent} from './list-pub/list-pub.component';
import {RouterModule} from '@angular/router';

import { FlexLayoutModule } from '@angular/flex-layout';
import {PublicationsRoutes} from './publications.routing';
import {DemoMaterialModule} from '../demo-material-module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';


@NgModule({
  declarations: [ListPubComponent,
    AddPubComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(PublicationsRoutes),
    DemoMaterialModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    FormsModule,
    Ng2SearchPipeModule,
  ]
})
export class PublicationsModule { }
