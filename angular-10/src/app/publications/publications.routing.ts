import { Routes } from '@angular/router';
import {AddPubComponent} from './add-pub/add-pub.component';
import {ListPubComponent} from './list-pub/list-pub.component';

export const PublicationsRoutes: Routes =
 [
  {

    path: '',
    pathMatch: 'full',
    redirectTo: 'publications',
  },
{
 path: '',
 
 children: [{
   path: '',
   pathMatch: 'full',
   component: ListPubComponent
 },
   {
     path: 'create',
     pathMatch: 'full',
     component: AddPubComponent
   },
   {
     path: ':id/edit',
     pathMatch: 'full',
     component: AddPubComponent
   },
   {
     path: 'delete',
     pathMatch: 'full',
     component: AddPubComponent
   }
 ,
{
 path: '**',
 redirectTo: '',
}]
},
{
 path: '**',
 redirectTo: '',
}
];
