import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DialogComponent } from 'app/material-component/dialog/dialog.component';
import { Membre } from 'app/models/membre.model';
import { AuthService } from 'app/services/auth.service';
import { MembreService } from 'app/services/membre.service';
import { Subject } from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import { ConfirmDialogComponent } from '../../../material-component/confirm-dialog/confirm-dialog.component';
import { Evts } from '../../../models/Evts.model';
import { EvenementsService } from '../../../services/evenements.service';

@Component({
  selector: 'app-evt-list',
  templateUrl: './evt-list.component.html',
  styleUrls: ['./evt-list.component.css']
})
export class EvtListComponent implements OnInit , OnDestroy{

  protected _onDestroy = new Subject<void>();
  displayedColumns: string[] = ['id','date', 'lieu','titre','actions'];
  dataSource: Evts[] = [];
  searchText:any;
  user:any
  membre!:Membre
  constructor(private evenementService :EvenementsService,
    private dialog: MatDialog,
    private authService: AuthService,
    private membreService :MembreService,
) {

  }
  ngOnDestroy(): void {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  ngOnInit(): void {
    this.fetchDataSource();
    
  }

  private fetchDataSource(): void {
    this.evenementService.getAllEvts().then(data => this.dataSource = data);
  }

  onRemoveAccount(id: any): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      hasBackdrop: true,
      disableClose: false,
    });

    dialogRef.componentInstance.confirmButtonColor = 'warn';

    dialogRef.afterClosed().pipe(takeUntil(this._onDestroy)).subscribe(isDeleteConfirmed => {
      console.log('removing: ', isDeleteConfirmed);
      if (isDeleteConfirmed) {
        this.evenementService.removeEvtById(id).then(() => this.fetchDataSource());
      }
    });
  }

  isAdmin():boolean{
    return this.authService.isAdmin();
  }

affecter(idmem:string, idevt:string): void {


  const dialogRef = this.dialog.open(DialogComponent, {
     hasBackdrop: true,
     disableClose: false,
   });
   dialogRef.componentInstance.confirmButtonColor = 'warn';
   dialogRef.componentInstance.message = `Etes-vous sûr pour cette affectation`;
 
   dialogRef.afterClosed().pipe(takeUntil(this._onDestroy)).subscribe(isDeleteConfirmed => {
   console.log('removing: ', isDeleteConfirmed);
   if (isDeleteConfirmed) {
     this.membreService.getCurrentUser().then(user =>{
        this.membre.id=user.id;
          this.membreService.affecterEvtMembre(user.id,idevt)});
 }
})
}

}
