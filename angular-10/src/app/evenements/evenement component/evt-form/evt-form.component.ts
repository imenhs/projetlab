import { Component, OnInit } from '@angular/core';
import { Evts } from '../../../models/Evts.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EvenementsService } from '../../../services/evenements.service';
import { MembreService } from 'app/services/membre.service';
@Component({
  selector: 'app-evt-form',
  templateUrl: './evt-form.component.html',
  styleUrls: ['./evt-form.component.css']
})
export class EvtFormComponent implements OnInit {

  currentItemId?: string;
  item?: Evts;
  form?: FormGroup;
  

  constructor( private router: Router,
    private activatedRoute: ActivatedRoute,
    private evenementsService :EvenementsService,
    private membreService: MembreService,

   ) { }

   ngOnInit(): void {
    this.currentItemId = this.activatedRoute.snapshot.params.id;
    if (!!this.currentItemId) {
      this.evenementsService.getEvtById(this.currentItemId).then(item => {
        this.item = item;
        this.initForm(item);
      });
    } else {
      this.initForm(null!);
    }
  }

  initForm(item: any): void {
    this.form = new FormGroup({
      titre: new FormControl(item?.titre, [Validators.required]),
    
      date: new FormControl(item?. date, [Validators.required]),
    lieu: new FormControl(item?.lieu, [Validators.required]),
      
    });
  }

  isFormInEditMode(): boolean {
    return !!this.currentItemId;
  }

  onSubmit(): void {
    console.log(this.form!.value);
    const objectToSubmit: Evts= {...this.item, ...this.form!.value};
    if (this.isFormInEditMode()) {
      this.evenementsService.updateEvt(this.currentItemId!, objectToSubmit).then(() => {
        this.previousState();
      });
    } else {
      this.evenementsService.createEvt(objectToSubmit).then(data => {
        this.membreService.getCurrentUser().then(user =>{
          this.membreService.affecterEvtMembre(user.id,data.id).then(
            data => {
              this.previousState()})
            })
      });
    }
  }

  previousState(): void {
    window.history.back();
  }
}