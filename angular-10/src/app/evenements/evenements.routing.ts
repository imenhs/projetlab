import { Routes } from '@angular/router';
import { AuthGuard } from 'app/services/auth.guard';
import { EvtFormComponent } from './evenement component/evt-form/evt-form.component';
import { EvtListComponent } from './evenement component/evt-list/evt-list.component';


export const EvenementsRoutes: Routes = 
  [

   /* {
      path: 'evts',
      children: [{
        path: '',
        pathMatch: 'full',
        component: EvtListComponent
      },
        {
        path: 'add',
        pathMatch: 'full',
        component: EvtFormComponent
        }
        ]
      },
       {
         path: 'evts/create',
         pathMatch: 'full',
         component: EvtFormComponent
       },
       {
         path: 'evts/:id/edit',
         pathMatch: 'full',
         component: EvtFormComponent
       },
    
    ]*/
 
  
    {
      path: '',
      pathMatch: 'full',
      redirectTo: 'evts',
    },

{
  path: '',
  children: [
    {
      path: '',
      pathMatch: 'full',
      component: EvtListComponent,
    },
    {
      path: 'create',
      pathMatch: 'full',
      component:  EvtFormComponent,
    },
    {
      path: ':id/edit',
      pathMatch: 'full',
      component:  EvtFormComponent,
    },
    {
      path: '**',
      redirectTo: '',
    }
  ]
},
{
  path: '**',
  redirectTo: 'evts',
}
];