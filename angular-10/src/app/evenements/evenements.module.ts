import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DemoMaterialModule } from '../demo-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { EvenementsRoutes } from './evenements.routing';
import { EvtFormComponent } from './evenement component/evt-form/evt-form.component';
import { EvtListComponent } from './evenement component/evt-list/evt-list.component';
import { ChartistModule } from 'ng-chartist';
import { AppRoutes } from '../app.routing';
import { Ng2SearchPipeModule } from 'ng2-search-filter';



@NgModule({
  declarations: [ EvtFormComponent,EvtListComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(EvenementsRoutes),
        RouterModule.forChild(AppRoutes),
        DemoMaterialModule,
        FlexLayoutModule,
        ReactiveFormsModule,
        ChartistModule,
        FormsModule,
      Ng2SearchPipeModule,
    ]
})
export class EvenementsModule { }
