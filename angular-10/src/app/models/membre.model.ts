
export interface Membre {
    id: string,
    cin: string,
    nom: string,
    prenom: string,
    dateNaissance: string,
    cv: string,
    photo:string,
    email:string,
    password:string,
    dateInscription?: string,
    sujet?: string,
    diplome?: string,
    etablissement?: string,
    grade?: string,
  encadrant?:string,
}
