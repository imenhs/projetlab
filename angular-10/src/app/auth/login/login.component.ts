import { Component, NgZone, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form!: FormGroup;
  public loginInvalid!: boolean;
  private formSubmitAttempt!: boolean;
  private returnUrl!: string;
  
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private ngZone: NgZone,

  ) { }

  async ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/game';

    this.form = this.fb.group({
      username: ['', Validators.email],
      password: ['', Validators.required]
    });

    /*if (await this.authService.checkAuthenticated()) {
      await this.router.navigate([this.returnUrl]);
    }*/
  }

  async onSubmit() {
    this.loginInvalid = false;
    this.formSubmitAttempt = false;
    if (this.form.valid ) {      
        const username = this.form.get('username')?.value;
        const password = this.form.get('password')?.value;
        this.authService.doLogin(username, password)
          .then(() => this.successRedirect())
          .catch(error => console.log(error));
      
    } else {
      this.formSubmitAttempt = true;
    }
  
  }
  tryGoogleLogin(): void {
    this.authService.doGoogleLogin()
      .then(() => this.successRedirect())
      .catch(error => console.log(error));
      
  }


  successRedirect(): void {
    // noinspection JSIgnoredPromiseFromCall
    if(this.form.get('username')?.value =='admin@admin.com'){
      this.ngZone.run(() => this.router.navigate(['/members']));
    }
    else{
      this.ngZone.run(() => this.router.navigate(['/profil']));

    }
  }


}
