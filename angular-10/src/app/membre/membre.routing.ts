import { Routes } from '@angular/router';
import { EnseignantFormComponent } from './enseignant-form/enseignant-form.component';
import { MembreListComponent } from './membre-list/membre-list.component';
import {EtudiantFormComponent} from './etudiant-form/etudiant-form.component';
import { AuthGuard } from 'app/services/auth.guard';
import {EtudiantListComponent} from './etudiant-list/etudiant-list.component';
import {ProfilComponent} from '../profil/profil.component';

export const MembreRoutes: Routes = [{
  path: '',
  pathMatch: 'full',
  redirectTo: 'members',
},
  {
    path: '',
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: MembreListComponent,
      },
      {
        path: 'enseignant-create',
        pathMatch: 'full',
        component: EnseignantFormComponent,
      },
      {
        path: 'student-create',
        pathMatch: 'full',
        component: EtudiantFormComponent,
      },
      {
        path: ':id/enseignant-edit',
        pathMatch: 'full',
        component: EnseignantFormComponent,
      },
      {
        path: ':id/student-edit',
        pathMatch: 'full',
        component: EtudiantFormComponent,
      },
      {
        path: ':id/profil',
        pathMatch: 'full',
        component: ProfilComponent,
      },
      {
        path: '**',
        redirectTo: '',
      }
    ],

  },

  {
    path: '**',
    redirectTo: 'members',
  },

];
