import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {MembreRoutes} from '../membre/membre.routing';

import { MembreListComponent } from './membre-list/membre-list.component';
import { DemoMaterialModule } from '../demo-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { EtudiantFormComponent } from './etudiant-form/etudiant-form.component';
import {EnseignantFormComponent} from './enseignant-form/enseignant-form.component';
import { EtudiantListComponent } from './etudiant-list/etudiant-list.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
@NgModule({
  declarations: [EnseignantFormComponent, MembreListComponent, EtudiantFormComponent, EtudiantListComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(MembreRoutes),
    DemoMaterialModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    FormsModule,
    Ng2SearchPipeModule
  ]
})
export class MembreModule { }
