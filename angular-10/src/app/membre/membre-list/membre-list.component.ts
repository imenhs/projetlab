import {Component, OnDestroy, OnInit} from '@angular/core';
import {Membre} from '../../models/membre.model';
import {MembreService} from '../../services/membre.service';


import {MatDialog} from "@angular/material/dialog";
import { ConfirmDialogComponent } from '../../material-component/confirm-dialog/confirm-dialog.component';

import {Subject} from "rxjs";
import {takeUntil} from "rxjs/operators";
import { AuthService } from 'app/services/auth.service';

@Component({
  selector: 'app-membre-list',
  templateUrl: './membre-list.component.html',
  styleUrls: ['./membre-list.component.css']
})
export class MembreListComponent implements OnInit, OnDestroy {
  protected _onDestroy = new Subject<void>();
  displayedColumns: string[] = ['id', 'cin', 'nom','prenom', 'email','type','profil','actions'];
  dataSource: Membre[] = [];

  searchText:any;
  constructor(private membreService :MembreService,
    private dialog: MatDialog,
    private authService: AuthService,
    ) {

  }
  ngOnDestroy(): void {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  ngOnInit(): void {
    this.fetchDataSource();
  }

  private fetchDataSource(): void {
    this.membreService.getAllMembres().then(data => this.dataSource = data);
    console.log(this.dataSource);
  }

  onRemoveAccount(id: any): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      hasBackdrop: true,
      disableClose: false,
    });

    dialogRef.componentInstance.confirmButtonColor = 'warn';

    dialogRef.afterClosed().pipe(takeUntil(this._onDestroy)).subscribe(isDeleteConfirmed => {
      console.log('removing: ', isDeleteConfirmed);
      if (isDeleteConfirmed) {
        this.membreService.removeMembreById(id).then(() => this.fetchDataSource());
      }
    });
  }

  isAdmin():boolean{
    return this.authService.isAdmin();
  }
}
