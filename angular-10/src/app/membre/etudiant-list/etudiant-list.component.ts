import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {Membre} from '../../models/membre.model';
import {MembreService} from '../../services/membre.service';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {DialogComponent} from '../../material-component/dialog/dialog.component';
import {takeUntil} from 'rxjs/operators';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-etudiant-list',
  templateUrl: './etudiant-list.component.html',
  styleUrls: ['./etudiant-list.component.css']
})
export class EtudiantListComponent implements OnInit, OnDestroy {
  protected _onDestroy = new Subject<void>();
  displayedColumns: string[] = ['id', 'cin', 'nom','prenom', 'email','affecter'];
  dataSource: Membre[] = [];
  user: any;
  idens!: string;
  membre!: Membre;


  searchText:any;
  constructor(private membreService :MembreService,private router: Router,
              private authService: AuthService, private dialog: MatDialog,) {

  }
  ngOnDestroy(): void {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  ngOnInit(): void {
    this.fetchDataSource();

  }

  private fetchDataSource(): void {
    this.membreService.getAllStudents().then(data => this.dataSource = data);
    console.log(this.dataSource);
  }
  CurrentUserId() : any{
    this.user = this.authService.userClaims;
    console.log(this.user.email);
    this.membreService.getMembreByEmail(this.user.email).then(data => {this.membre = data
    });
    if (this.membre != null)
    {return this.membre.id;}
    else return '0';
}
  affecter(idetd:string, idens:string): void {


 const dialogRef = this.dialog.open(DialogComponent, {
    hasBackdrop: true,
    disableClose: false,
  });
  dialogRef.componentInstance.confirmButtonColor = 'warn';
  dialogRef.componentInstance.message = `Etes-vous sûr pour cette affectation`;

  dialogRef.afterClosed().pipe(takeUntil(this._onDestroy)).subscribe(isDeleteConfirmed => {
  console.log('removing: ', isDeleteConfirmed);
  if (isDeleteConfirmed) {
   this.membreService.affecterStudentMembre(idetd,idens).then(() => this.fetchDataSource());
  }

  });
}
}
