import { Component, OnInit } from '@angular/core';
import {Membre} from '../../models/membre.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {MembreService} from '../../services/membre.service';

@Component({
  selector: 'app-etudiant-form',
  templateUrl: './etudiant-form.component.html',
  styleUrls: ['./etudiant-form.component.css']
})
export class EtudiantFormComponent implements OnInit {
  currentItemId?: string;
  event: any;
  form?: FormGroup;
  userCv: any;
  userPhoto: any;
  public pdfPath! : string;
  imgURL: any;
  pdfURL: any;
  public message: string | undefined;
  constructor( private router: Router,
               private activatedRoute: ActivatedRoute,
               private membreService: MembreService,
  ) { }

  ngOnInit(): void {
    this.currentItemId = this.activatedRoute.snapshot.params.id;
    if (!!this.currentItemId) {
      this.membreService.getMembreById(this.currentItemId).then(item => {
        this.initForm(item);
      });
    } else {
      this.initForm(null);
    }
  }

  initForm(item: any): void {
    this.form = new FormGroup({
      cin: new FormControl(item?.cin, [Validators.required]),
      nom: new FormControl(item?.nom, [Validators.required]),
      prenom: new FormControl(item?.prenom, [Validators.required]),
      photo: new FormControl(item?.photo, [Validators.required]),
      cv: new FormControl(item?.cv, [Validators.required]),
      dateInscription: new FormControl(new Date()),
      dateNaissance: new FormControl(item?.dateNaissance, [Validators.required]),
      password: new FormControl(item?.password, [Validators.required]),
      email: new FormControl(item?.email, [Validators.required]),
      sujet:new FormControl(item?.sujet, [Validators.required]),
      diplome:new FormControl(item?.diplome, [Validators.required]),
    });
  }

  isFormInEditMode(): boolean{
    return !!this.currentItemId;
  }


  onSubmit(): void {
    const formData = new  FormData();
    const etudiant = this.form!.value;
    formData.append('etudiant',JSON.stringify(etudiant));
    formData.append('photo',this.userPhoto);
    formData.append('cv',this.userCv);
    if (this.isFormInEditMode()) {
      this.membreService.updateStudent(this.currentItemId!,etudiant).then(() => {
        this.router.navigate(['./members']);
      });
    } else {
      this.membreService.createStudent(formData).then(() => {
        this.router.navigate(['./members']);
      });
    }
  }


  onSelectFile(event: any) {
    if (this.currentItemId !=null){}

    if (event.target.files && event.target.files[0]) {
      const photo = event.target.files[0];
      this.userPhoto = photo;
      var mimeType = event.target.files[0].type;
      if (mimeType.match(/image\/*/) == null) {
        this.message = "Only images are supported.";
        return;
      }
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.imgURL = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }
  onSelectCV(event: any) {
    if (event.target.files.length > 0)
    { const pdf = event.target.files[0];
      this. userCv = pdf;
      // this.f['profile'].setValue(file);
      var mimeType = event.target.files[0].type;
      if (mimeType.match(/pdf\/*/) == null) {
        this.message = "Only images are supported.";
        return;
      }
      var reader = new FileReader();
      this.pdfPath = pdf;
      reader.readAsDataURL(pdf);
      reader.onload = (_event) => {
        this.pdfURL = reader.result;
      }
    }
  }
}
