import { Routes } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { ProfilComponent } from './profil/profil.component';
import { AuthGuard } from './services/auth.guard';
import {EtudiantListComponent} from './membre/etudiant-list/etudiant-list.component';

export const AppRoutes: Routes = [



      {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full'
      },
      {
        path: 'evts',
        loadChildren: () => import('./evenements/evenements.module').then(m => m.EvenementsModule)
      },

      {
        path: '',
        loadChildren:
          () => import('./material-component/material.module').then(m => m.MaterialComponentsModule)
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'members',
        loadChildren: () => import('./membre/membre.module').then(m => m.MembreModule)
      },
      {
        path: 'outils',
        loadChildren:() => import('./outil/outil.module').then(m => m.OutilModule)
      },

      {
        path: 'publications',
        loadChildren:
          () => import('./publications/publications.module').then(m => m.PublicationsModule)
      },
      {
        path: 'login',
        pathMatch: 'full',
        component: LoginComponent
      },

      {
        path: 'profil',
        canActivate: [AuthGuard],
        pathMatch: 'full',
        component: ProfilComponent,
      },
  {
    path: 'etudiants',
    canActivate: [AuthGuard],
    pathMatch: 'full',
    component: EtudiantListComponent,
  },

];
