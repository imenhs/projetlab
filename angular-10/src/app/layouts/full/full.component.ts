import { MediaMatcher } from '@angular/cdk/layout';
import {ChangeDetectorRef, Component,OnDestroy,AfterViewInit, OnInit, Input} from '@angular/core';
import { Membre } from 'app/models/membre.model';
import { AuthService } from 'app/services/auth.service';
import { MembreService } from 'app/services/membre.service';
import { MenuItems } from '../../shared/menu-items/menu-items';


/** @title Responsive sidenav */
@Component({
  selector: 'app-full-layout',
  templateUrl: 'full.component.html',
  styleUrls: []
})
export class FullComponent implements OnDestroy, AfterViewInit, OnInit {
  mobileQuery: MediaQueryList;
  isLoggedIn:any;
  user: any;
  membre!: Membre;
  public path=this.membreService.path+'/MembrePhoto/';

  private _mobileQueryListener: () => void;

  constructor(
    private authService: AuthService,
    private membreService: MembreService,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    public menuItems: MenuItems
  ) {
    this.mobileQuery = media.matchMedia('(min-width: 768px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit(): void {  
    this.authService.userClaims$.subscribe(user => {
      this.user = !!user ? user : null;
      this.isLoggedIn = !!user; 
      if(user){
        this.membreService.getMembreByEmail(user.email).then(data =>{this.membre = data,
          console.log(this.membre)})
      }
      
    });

  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }
  ngAfterViewInit() {}


}
