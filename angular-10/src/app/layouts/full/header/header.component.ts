import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Membre } from 'app/models/membre.model';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: []
})
export class AppHeaderComponent implements OnInit {
  isLoggedIn!: boolean;
  user: any;
  @Input()
  membre?: Membre | null;
  @Input()
  path!:String;


  constructor(
    private authService: AuthService,
    private router: Router,
    
  ) {
    
  }

  ngOnInit(): void {
    this.user = this.authService.userClaims
    this.authService.userClaims$.subscribe(user => {
      this.user = !!user ? user : null;
      this.isLoggedIn = !!user;
    });
    }

  
    

  logout(): void {
    this.authService.doLogout().finally(() => {
      this.router.navigate(['/login']);
      this.membre=null;
    });
  }


  
}
