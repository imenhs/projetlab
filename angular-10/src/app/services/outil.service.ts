import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Outil } from '../models/outil.model';

@Injectable({
  providedIn: 'root'
})
export class OutilService {
  private path = `${environment.gatewayEndpoint}/outil-service`;
  public placeholderOutils: Outil[] = [];

  constructor(    
    private httpClient: HttpClient,
    ) { 
    }
  saveOutil(outil:any) {
    const outilToSave = {
      id: outil.id ?? Math.ceil(Math.random() * 10000).toString(),
      createdDate: outil.createdDate ?? new Date().toISOString(), ...outil
    };
    this.placeholderOutils= [outilToSave, ...this.placeholderOutils.filter(item => item.id !== outil.id)];

    return new Promise(resolve => resolve(outilToSave));
  }
  
  getAllOutils(): Promise<Outil[]> {
    return this.httpClient.get<Outil[]>(`${this.path}/outils`).toPromise();
     //return new Promise(resolve => resolve(this.placeholderOutils));
  }

  getOutilById(id: number): Promise<Outil> {
    return this.httpClient.get<Outil>(`${this.path}/outil/${id}`).toPromise();
    //  return new Promise(resolve => resolve(
    //    this.placeholderOutils.filter(item => item.id === id)[0] ?? null
    //  ));
  }
  removeOutilById(id: number): Promise<void> {
     return this.httpClient.delete<void>(`${this.path}/outil/${id}`).toPromise();
    // this.placeholderOutils = this.placeholderOutils.filter(item => item.id !== id);
    // return new Promise(resolve => resolve());
  }

  createOutil(outil: Outil): Promise<Outil> {
    return this.httpClient.post<Outil>(`${this.path}/outil`, outil).toPromise();
  }

  updateOutil(id: number, outil: Outil): Promise<Outil> {
    return this.httpClient.put<Outil>(`${this.path}/outil/${id}`, outil).toPromise();
  }

  
}
