import { Injectable } from '@angular/core';
import {Publication} from '../models/publications.model';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Membre} from '../models/membre.model';

@Injectable({
  providedIn: 'root'
})
export class PublicationsService {
  public path = `${environment.gatewayEndpoint}/publication-service`;
  constructor(private httpClient : HttpClient) { }

  getAllPublications(): Promise<Publication[]> {
    return this.httpClient.get<Publication[]>(`${this.path}/publications`).toPromise();
  }

  getPublicationById(id: string): Promise<Publication> {
    return this.httpClient.get<Publication>(`${this.path}/publications/${id}`).toPromise();
  }

  createPublication(formData: FormData): Promise<Membre> {
    return this.httpClient.post<Membre>(`${this.path}/publications`, formData).toPromise();
  }

  updatePublication(id: string, publication: Publication): Promise<Membre> {
    return this.httpClient.put<Membre>(`${this.path}/publications/${id}`, publication).toPromise();
  }
  removePublicationById(id: string): Promise<void> {
  return this.httpClient.delete<void>(`${this.path}/publications/${id}`).toPromise();

  }
}
