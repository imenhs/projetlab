import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Evts } from '../models/Evts.model';

@Injectable({
  providedIn: 'root'
})
export class EvenementsService {

  private path = `${environment.gatewayEndpoint}/evenement-service`;
  public placeholderMembers: Evts[] = [{
    "id": "123456",
    "titre": "cyberSecurity",
    "date":"2020-11-02T09:09:00Z",
    "lieu":"ENIS"
  },
  {
    "id": "123",
    "titre": "problemsolving",
    "date":"2021-11-02T09:09:00Z",
    "lieu":"ENIS"
  },
  ]
  
  constructor(private httpClient: HttpClient,) { }
    getAllEvts(): Promise<Evts[]> {
      return this.httpClient.get<Evts[]>(`${this.path}/evenements`).toPromise();
}
getEvtById(id: string): Promise<Evts> {
  return this.httpClient.get<Evts>(`${this.path}/evenement/${id}`).toPromise();
}
updateEvt(id: string, evts: Evts) : Promise<Evts> {
  return this.httpClient.put<Evts>(`${this.path}/evenement/${id}`, evts).toPromise();}

  createEvt(evts: Evts): Promise<Evts> {
    return this.httpClient.post<Evts>(`${this.path}/evenement`, evts).toPromise();
}
removeEvtById(id: string): Promise<void> {
return this.httpClient.delete<void>(`${this.path}/evenement/${id}`).toPromise();

}
saveEvt(evts: any): Promise<Evts> {
  return this.httpClient.post<Evts>(`${this.path}/evenement`, evts).toPromise();
  }}
 
