import { Injectable } from '@angular/core';
import { Membre } from '../models/membre.model';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';
import { Outil } from '../models/outil.model';
import { AuthService } from './auth.service';
import { Publication } from '../models/publications.model';
import { Evts } from '../models/Evts.model';

@Injectable({
  providedIn: 'root'
})
export class MembreService {
  public path = `${environment.gatewayEndpoint}/membre-service`;

  constructor(private httpClient: HttpClient,
              private authService: AuthService,) {
  }

  getCurrentUser(): Promise<Membre> {
    const user = this.authService.userClaims;
    return this.getMembreByEmail(user.email)
  }

  getAllMembres(): Promise<Membre[]> {
    return this.httpClient.get<Membre[]>(`${this.path}/membres`).toPromise();
  }

  getMembreById(id: string): Promise<Membre> {
    return this.httpClient.get<Membre>(`${this.path}/membres/${id}`).toPromise();
  }

  getMembreByEmail(email: string): Promise<Membre> {
    return this.httpClient.get<Membre>(`${this.path}/membres/email/${email}`).toPromise();
  }

  createStudent(formData: FormData): Promise<Membre> {
    return this.httpClient.post<Membre>(`${this.path}/membres/etudiant`, formData).toPromise();
  }

  updateStudent(id: string, membre: Membre): Promise<Membre> {
    return this.httpClient.put<Membre>(`${this.path}/membres/etudiant/${id}`, membre).toPromise();

  }

  getOutilsByCurrentUser(id: string): Promise<Outil[]> {
    return this.httpClient.get<Outil[]>(`${this.path}/outils/developpeur/${id}`).toPromise();
  }


  createData(formData: FormData): Promise<Membre> {
    return this.httpClient.post<Membre>(`${this.path}/membres/enseignant`, formData).toPromise();
  }

  updateData(id: string, membre: Membre): Promise<Membre> {
    return this.httpClient.put<Membre>(`${this.path}/membres/enseignant/${id}`, membre).toPromise();
  }



  affecterOutilMembre(idmem: string, idout: number): Promise<Membre>{
    const params = new HttpParams()
      .set('idmem', idmem)
      .set('idout', String(idout))
    return this.httpClient.get<any>(`${this.path}/membres/outil`, {params}).toPromise();
  }

  affecterEvtMembre(idmem: string, idevt: string): Promise<Membre>{
    const params = new HttpParams()
      .set('idmem', idmem)
      .set('idevt', idevt)
    return this.httpClient.post<any>(`${this.path}/membres/evenement`, params).toPromise();
  }

  affecterPubMembre(idmem: string, idpub: string): Promise<Membre>{
    const params = new HttpParams()
      .set('idmem', idmem)
      .set('idpub', idpub)
    return this.httpClient.get<any>(`${this.path}/membres/publication`,  {params}).toPromise();
  }

  affecterStudentMembre(idetd: string, idens: string): Promise<Membre> {
    const params = new HttpParams()
      .set('idetd', idetd)
      .set('idens', idens);
    return this.httpClient.put<Membre>(`${this.path}/membres/etudiant`, params).toPromise();
  }

  getAllStudents(): Promise<Membre[]> {
    return this.httpClient.get<Membre[]>(`${this.path}/etudiants`).toPromise();
  }

  getStudentsByCurrentUser(id: string) {
    return this.httpClient.get<Membre[]>(`${this.path}/students/encadrant/${id}`).toPromise();
  }

  getPublicationsByCurrentUser(id: string): Promise<Publication []>{
    return this.httpClient.get<Publication []>(`${this.path}/publications/auteur/${id}`).toPromise();
  }
  getEvtsByCurrentUser(id: string): Promise<Evts []>{
    return this.httpClient.get<Evts[]>(`${this.path}/evenements/participant/${id}`).toPromise();
  }

  removeMemberOutil(id: string): Promise<void>{
    return this.httpClient.delete<void>(`${this.path}/outil/${id}`).toPromise();

  }
  removeMemberPub(id: string): Promise<void>{
    return this.httpClient.delete<void>(`${this.path}/publication/${id}`).toPromise();

  }
  removeMemberEvt(id: string): Promise<void>{
    return this.httpClient.delete<void>(`${this.path}/evenement/${id}`).toPromise();

  }

 removeMembreById(id: string): Promise<void> {
  return this.httpClient.delete<void>(`${this.path}/membres/${id}`).toPromise();
}

AnnulerAffectation(id: string,idens: string): Promise<Membre[]> {
  const params = new HttpParams()
    .set('id', id)
    .set('idens', idens);
  return this.httpClient.put<Membre[]>(`${this.path}/encadrant/student`,params).toPromise();
}
}
