import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DialogComponent } from '../material-component/dialog/dialog.component';
import { Membre } from '../models/membre.model';
import { Outil } from '../models/outil.model';
import { MembreService } from '../services/membre.service';
import { OutilService } from '../services/outil.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';
import { Evts } from '../models/Evts.model';
import { Publication } from '../models/publications.model';
import { ConfirmDialogComponent } from '../material-component/confirm-dialog/confirm-dialog.component';
import { EvenementsService } from '../services/evenements.service';
import { PublicationsService } from '../services/publications.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {
  isLoggedIn!: boolean;
  user: any;
  membre!: Membre;
  outilsData?:Outil[];
  studentsData?:Membre[];
  PubsData?:Publication[];
  EvtsData?:Evts[];
  currentItemId!: string;
  downloadPath=this.membreService.path + '/files/';
  public path=this.membreService.path+'/MembrePhoto/';
  photo:any;

  protected _onDestroy = new Subject<void>();

  constructor(
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
    private membreService: MembreService,
    private dialog: MatDialog,
    private outilService : OutilService,
    private evenementService :EvenementsService,
    private publicationService : PublicationsService,
  ) {}

  ngOnInit(): void {
    this.currentItemId = this.activatedRoute.snapshot.params.id;
    /*if (!!this.currentItemId) {
      this.membreService.getMembreById(this.currentItemId).then(item => {
        this.membre = item;
      });
    }*/
    this.fetchCurrentUser();

  }
  fetchCurrentUser() {
    if(!!this.currentItemId) {
      this.membreService.getMembreById(this.currentItemId).then(item => {
        this.membre = item;
        this.fetchOutilsCurrentUser(this.membre.id),
          this.fetchStudentsCurrentUser(this.membre.id);
        this.fetchPublicationsCurrentUser(this.membre.id);
        this.fetchEvtsCurrentUser(this.membre.id)
      });
    }
    else {
      this.user = this.authService.userClaims;
      this.membreService.getMembreByEmail(this.user.email).then(data => {
        this.membre = data,
          console.log(this.membre),

          this.fetchOutilsCurrentUser(this.membre.id),
          this.fetchStudentsCurrentUser(this.membre.id);
        this.fetchPublicationsCurrentUser(this.membre.id);
        this.fetchEvtsCurrentUser(this.membre.id)
      });
    }

  }

  fetchOutilsCurrentUser(id:string){
    this.membreService.getOutilsByCurrentUser(id).then(data => this.outilsData = data);

  }
  fetchStudentsCurrentUser(id:string){
    this.membreService.getStudentsByCurrentUser(id).then(data => this.studentsData = data);
  }
  fetchPhoto(id:string){
    this.photo =this.membreService.path+'/MembrePhoto/'+id;
    console.log(this.photo)
  }

  onRemoveOutil(id: any,idmembre:string): void {

    const dialogRef = this.dialog.open(DialogComponent, {
      hasBackdrop: true,
      disableClose: false,
    });
      dialogRef.componentInstance.confirmButtonColor = 'warn';
      dialogRef.componentInstance.message = `Etes-vous sure de vouloir supprimer l'outil ${id}`;

    dialogRef.afterClosed().pipe(takeUntil(this._onDestroy)).subscribe(isDeleteConfirmed => {
      console.log('removing: ', isDeleteConfirmed);
      if (isDeleteConfirmed) {
        this.membreService.removeMemberOutil(id).then(()=>
        this.outilService.removeOutilById(id).then(() => this.fetchOutilsCurrentUser(idmembre)));
      }
    });
  }

  onRemoveEvt(id: any,idmembre:string): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      hasBackdrop: true,
      disableClose: false,
    });
    dialogRef.componentInstance.confirmButtonColor = 'warn';
    dialogRef.afterClosed().pipe(takeUntil(this._onDestroy)).subscribe(isDeleteConfirmed => {
      console.log('removing: ', isDeleteConfirmed);
      if (isDeleteConfirmed) {
        this.membreService.removeMemberEvt(id).then(()=>
        this.evenementService.removeEvtById(id).then(() => this.fetchEvtsCurrentUser(idmembre)));
      }
    });
  }

  onRemovePub(id: string,idmembre:string) : void {
    const dialogRef = this.dialog.open(DialogComponent, {
      hasBackdrop: true,
      disableClose: false,
    });

    dialogRef.componentInstance.confirmButtonColor = 'warn';

    dialogRef.afterClosed().pipe(takeUntil(this._onDestroy)).subscribe(isDeleteConfirmed => {
      console.log('removing: ', isDeleteConfirmed);
      if (isDeleteConfirmed) {
        this.membreService.removeMemberPub(id).then(()=>
        this.publicationService.removePublicationById(id).then(() => this.fetchPublicationsCurrentUser(idmembre) ));
      }
    });
  }


  fetchPublicationsCurrentUser(id:string){
    this.membreService.getPublicationsByCurrentUser(id).then(data => this.PubsData = data);

  }

  fetchEvtsCurrentUser(id:string){
    this.membreService.getEvtsByCurrentUser(id).then(data => this.EvtsData = data);

  }

  AnnulerAffectation(id: string,idens:string): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      hasBackdrop: true,
      disableClose: false,
    });
    dialogRef.componentInstance.confirmButtonColor = 'warn';
    dialogRef.componentInstance.message = `Etes-vous sûr pour cette annulation`;

    dialogRef.afterClosed().pipe(takeUntil(this._onDestroy)).subscribe(isDeleteConfirmed => {
    console.log('removing: ', isDeleteConfirmed);
    if (isDeleteConfirmed) {

        this.membreService.AnnulerAffectation(id,idens).then(() => this.fetchStudentsCurrentUser(this.membre.id));
      }
    });
  }

  isCurrentUser():boolean{
    if (this.currentItemId==this.membre.id){
      return false
    }else{
      return  true
    }
  }
}
