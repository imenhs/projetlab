import { LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DemoMaterialModule } from 'app/demo-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ChartistModule } from 'ng-chartist';
import { OutilListComponent } from './outil-list/outil-list.component';
import { OutilFormComponent } from './outil-form/outil-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {OutilRoutes } from './outil.routing';
import { Ng2SearchPipeModule } from 'ng2-search-filter';


@NgModule({
  declarations: [OutilListComponent, OutilFormComponent],
  imports: [
    CommonModule,
    DemoMaterialModule,
    FlexLayoutModule,
    ChartistModule,
    RouterModule.forChild(OutilRoutes),
    ReactiveFormsModule,
    FormsModule,
    Ng2SearchPipeModule,
  ]
})
export class OutilModule { }
