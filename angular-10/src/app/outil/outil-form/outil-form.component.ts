import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Outil } from 'app/models/outil.model';
import { MembreService } from 'app/services/membre.service';
import { OutilService } from 'app/services/outil.service';

@Component({
  selector: 'app-outil-form',
  templateUrl: './outil-form.component.html',
  styleUrls: ['./outil-form.component.css']
})
export class OutilFormComponent implements OnInit {
  outil!: Outil  ;
  currentItemId!: number;
  form!: FormGroup;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private outilService: OutilService,
    private membreService: MembreService

  ) {

  }

  ngOnInit(): void {
    this.currentItemId = this.activatedRoute.snapshot.params.id;
    if (!!this.currentItemId) {
      this.outilService.getOutilById(this.currentItemId).then(item => {
        this.outil = item;
        this.initForm(item);
      });
    } else {
      this.initForm(null);
    }
  }
  initForm(item:  Outil | null): void {
    this.form = new FormGroup({
      source: new FormControl(item?.source , [Validators.required]),
      //dateCreation: new FormControl(item?.dateCreation, [Validators.required]),
      dateCreation: new FormControl(item?.dateCreation ?? new Date(), [Validators.required]),

    });
  }

  onSubmit(): void {
    console.log(this.form.value);
    const objectToSubmit: Outil = {...this.outil, ...this.form.value};
    if (this.isFormInEditMode()) {
      this.outilService.updateOutil(this.currentItemId, objectToSubmit).then(() => {
        this.previousState();
      });
    } else {
      this.outilService.createOutil(objectToSubmit).then(data => {
        this.membreService.getCurrentUser().then(user =>{
          this.membreService.affecterOutilMembre(user.id,data.id).then(
            data => {
              this.previousState()})

        })

      });
    }
  }

  previousState(): void {
    window.history.back();
  }
  isFormInEditMode(): boolean {
    return !!this.currentItemId;

  }

}
