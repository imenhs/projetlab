import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'app/services/auth.guard';
import { OutilFormComponent } from './outil-form/outil-form.component';
import { OutilListComponent } from './outil-list/outil-list.component';


export const OutilRoutes: Routes = [
  
  {
    path: '',
    children: [
      {
        path: '',
        pathMatch: 'full',
        component:OutilListComponent,
      },
      {
        path: 'create',
        pathMatch: 'full',
        component: OutilFormComponent,
      },
      {
        path: ':id/edit',
        pathMatch: 'full',
        component: OutilFormComponent,
      },
      
      
    ]
  },
 
  
];
