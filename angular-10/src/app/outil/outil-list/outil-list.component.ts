import { Component, OnDestroy, OnInit } from '@angular/core';
import {OutilService} from '../../services/outil.service'
import { Outil } from 'app/models/outil.model';
import { DialogComponent } from 'app/material-component/dialog/dialog.component';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { AuthService } from 'app/services/auth.service';

@Component({
  selector: 'app-outil-list',
  templateUrl: './outil-list.component.html',
  styleUrls: ['./outil-list.component.css']
})
export class OutilListComponent implements OnInit, OnDestroy {
  protected _onDestroy = new Subject<void>();
  
  dataSource:Outil[]=[];
  searchText:any;

  constructor(
    private outilService : OutilService,
    private dialog: MatDialog,
    private authService: AuthService,
  ) {

   }
  ngOnDestroy(): void {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  ngOnInit(): void {
    this.fetchDataSource();
    
  }

  private fetchDataSource(): void{
    this.outilService.getAllOutils().then(data => this.dataSource = data);

  }
  onRemoveOutil(id: any): void {

    const dialogRef = this.dialog.open(DialogComponent, {
      hasBackdrop: true,
      disableClose: false,
    });
      dialogRef.componentInstance.confirmButtonColor = 'warn';
      dialogRef.componentInstance.message = `Etes-vous sure de vouloir supprimer l'outil ${id}`;

    dialogRef.afterClosed().pipe(takeUntil(this._onDestroy)).subscribe(isDeleteConfirmed => {
      console.log('removing: ', isDeleteConfirmed);
      if (isDeleteConfirmed) {
        this.outilService.removeOutilById(id).then(() => this.fetchDataSource());
      }
    });
  }

  isAdmin():boolean{
    return this.authService.isAdmin();
  }


}
