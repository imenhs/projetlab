import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent {
  public title = 'Vous êtes sûr?';
  public message = 'Vous êtes sûrs que vous pouvez supprimer cet element?';
  public confirmButtonLabel = 'Confirmer';
  public confirmButtonColor = 'accent';
  public cancelButtonLabel = 'Cancel';
  constructor(dialogRef: MatDialogRef<DialogComponent>) { }

}
