import {NgModule} from '@angular/core';


import {FlexLayoutModule} from '@angular/flex-layout';
import {ConfirmDialogComponent} from "./confirm-dialog.component";

@NgModule({
  declarations: [
    ConfirmDialogComponent
  ],
  imports: [
 
    FlexLayoutModule,
  ],
  entryComponents: [
    ConfirmDialogComponent
  ],
})
export class ConfirmDialogModule {
}
