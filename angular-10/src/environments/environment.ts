// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  gatewayEndpoint: 'http://localhost:9999',
  admin: 'admin@admin.com',

  firebase : {
    apiKey: "AIzaSyDYXvQx0GeFzKJzXknRs45quRKAanHwuZ0",
    authDomain: "enis-lab-s1.firebaseapp.com",
    projectId: "enis-lab-s1",
    storageBucket: "enis-lab-s1.appspot.com",
    messagingSenderId: "852313437816",
    appId: "1:852313437816:web:fe6b8ab2dd66c5d6b79f8d",
    measurementId: "G-YETNLDD4JW"
  }
};
