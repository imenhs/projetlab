package com.example.demo.entities;

import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@SuppressWarnings("serial")
@Embeddable
@Data @AllArgsConstructor @NoArgsConstructor
public class Membre_Outil_Ids implements Serializable {
	private Long outil_id;
	private Long developpeur_id;

}
