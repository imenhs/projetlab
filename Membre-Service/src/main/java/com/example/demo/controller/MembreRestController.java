package com.example.demo.controller;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import javax.servlet.ServletContext;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParseException;
import org.springframework.core.io.Resource;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.EvenementBean;
import com.example.demo.OutilBean;
import com.example.demo.PublicationBean;
import com.example.demo.dao.EtudiantRepository;
import com.example.demo.dao.MemberRepository;
import com.example.demo.entities.EnseignantChercheur;
import com.example.demo.entities.Etudiant;
import com.example.demo.entities.Membre;
import com.example.demo.proxies.EvenementProxy;
import com.example.demo.proxies.PublicationProxy;
import com.example.demo.service.IMemberService;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
@RestController
public class MembreRestController {
	@Autowired
	IMemberService iMemberService;
	@Autowired
	PublicationProxy publicationproxy;
	@Autowired
	EvenementProxy evproxy;
	@Autowired  
	ServletContext context;
	@Autowired
	MemberRepository memberRepository;
	@Autowired
	EtudiantRepository etudiantRepository;
	
	@GetMapping(value = "/membres")
	public List<Membre> findAllmembres()
	{
		return iMemberService.findAll();
	}

	@GetMapping(value = "/membres/{id}")
	public Membre findoneMembre(@PathVariable Long id)
	{
		return iMemberService.findMember(id);
	}
	
	@GetMapping(value = "/membres/email/{email}")
	public Membre findMembreByEmail(@PathVariable String email)
	{
		return iMemberService.findByEmail(email);
	}
	
	@GetMapping(value = "/membres/cin/{cin}")
	public Membre findMembreByCin(@PathVariable String cin)
	{
		return iMemberService.findByEmail(cin);
	}
	
	
	@GetMapping(value="/membres/outil")
	public void createOutilByuser(@RequestParam ("idmem") Long idmem , @RequestParam("idout") Long idout )
	{
	        iMemberService.affecterDevelopToOutil(idmem, idout );
	}
	
	@PostMapping(value = "/membres/etudiant")
	public Membre createEtudiant (@RequestParam("photo") MultipartFile photo,@RequestParam("cv") MultipartFile cv,
			 @RequestParam("etudiant") String etudiant) throws JsonParseException , JsonMappingException , Exception
	 {	System.out.println(etudiant);
		 System.out.println("Ok .............");
       Membre etd = new ObjectMapper().readValue(etudiant, Etudiant.class);
       boolean isExitImg = new File(context.getRealPath("/webapp/Images/")).exists();
       boolean isExitPdf = new File(context.getRealPath("/webapp/CV/")).exists();
       if (!isExitImg)
       {
       	new File (context.getRealPath("/Images/")).mkdir();
 
       	System.out.println("mkdir Images Etd....");
       }
       if (!isExitPdf)
       {
       
       	new File (context.getRealPath("/CV/")).mkdir();
       	System.out.println("mkdir CV Etd.....");
       }
       String Photoname = photo.getOriginalFilename();
       String Cvname = cv.getOriginalFilename();
       String newPhotoName = FilenameUtils.getBaseName(Photoname)+"."+FilenameUtils.getExtension(Photoname);
       String newCvName = FilenameUtils.getBaseName(Cvname)+"."+FilenameUtils.getExtension(Cvname);
       File serverPhoto = new File (context.getRealPath("/Images/"+File.separator+newPhotoName));
       File serverCv = new File (context.getRealPath("/CV/"+File.separator+newCvName));
       try
       {
       	System.out.println("Image");
       	 FileUtils.writeByteArrayToFile(serverPhoto,photo.getBytes());
       	System.out.println("CV");
       	 FileUtils.writeByteArrayToFile(serverCv,cv.getBytes());
       	 
       }catch(Exception e) {
       	e.printStackTrace();
       }

       etd.setPhoto(newPhotoName);
       etd.setCv(newCvName);
       return iMemberService.addMember(etd);
       
	 }
	@PostMapping(value = "/membres/enseignant")
	 public Membre createEnseignant (@RequestParam("photo") MultipartFile photo,@RequestParam("cv") MultipartFile cv,
			 @RequestParam("enseignant") String enseignant) throws JsonParseException , JsonMappingException , Exception
	 {	System.out.println(enseignant);
		 System.out.println("Ok .............");
        Membre ens = new ObjectMapper().readValue(enseignant, EnseignantChercheur.class);
       
        boolean isExitImg = new File(context.getRealPath("/webapp/Images/")).exists();
        boolean isExitPdf = new File(context.getRealPath("/webapp/CV/")).exists();
        if (!isExitImg)
        {
        	new File (context.getRealPath("/Images/")).mkdir();
  
        	System.out.println("mkdir Images........");
        }
        if (!isExitPdf)
        {
        
        	new File (context.getRealPath("/CV/")).mkdir();
        	System.out.println("mkdir CV.........");
        }
        String Photoname = photo.getOriginalFilename();
        String Cvname = cv.getOriginalFilename();
        String newPhotoName = FilenameUtils.getBaseName(Photoname)+"."+FilenameUtils.getExtension(Photoname);
        String newCvName = FilenameUtils.getBaseName(Cvname)+"."+FilenameUtils.getExtension(Cvname);
        File serverPhoto = new File (context.getRealPath("/Images/"+File.separator+newPhotoName));
        File serverCv = new File (context.getRealPath("/CV/"+File.separator+newCvName));
        try
        {
        	System.out.println("Image");
        	 FileUtils.writeByteArrayToFile(serverPhoto,photo.getBytes());
        	 FileUtils.writeByteArrayToFile(serverCv,cv.getBytes());
        	 
        }catch(Exception e) {
        	e.printStackTrace();
        }

        ens.setPhoto(newPhotoName);
        ens.setCv(newCvName);
        return iMemberService.addMember(ens);
        
	 }
	 @GetMapping(path="/MembrePhoto/{id}")
	 public byte[] getPhoto(@PathVariable("id") Long id) throws Exception{
		 Membre membre   = memberRepository.findById(id).get();
		 return Files.readAllBytes(Paths.get(context.getRealPath("/Images/")+membre.getPhoto()));
	 }

	
	@PutMapping(value="/membres/etudiant/{id}")
	public Membre updatemembre(@PathVariable Long id, @RequestBody Etudiant p)
	{
		p.setId(id);
		return iMemberService.updateMember(p);
	}


	@PutMapping(value="/membres/enseignant/{id}")
	public Membre updateMembre(@PathVariable Long id, @RequestBody EnseignantChercheur p)
	{
		p.setId(id);
	      return iMemberService.updateMember(p);
	}
	@DeleteMapping(value="/membres/{id}")
	public void deleteMembre(@PathVariable Long id)
	{
		 iMemberService.removeEnseignant(id);
	}

	@DeleteMapping(value="/outil/{idout}")
	public void deleteOutilMembre(@PathVariable Long idout)
	{
		 iMemberService.deleteOutilMember(idout);
	}

	@PutMapping(value="/membres/etudiant")
	public Membre affecter(@RequestParam Long idetd , @RequestParam Long idens )
	{
		
	       return iMemberService.affecterencadrantToetudiant(idetd, idens);
	}
	@GetMapping("/evenements")
	public CollectionModel<EvenementBean> listerevenement()
	{
		return evproxy.listeDesEvenements();
		
	}
	@GetMapping("/evenements/{id}")
	public EntityModel<EvenementBean> listerunevenement(@PathVariable Long id)
	{
		return evproxy.recupererUnEvenement(id);
		
	}
	@GetMapping("/evenements/participant/{id}")
	public List<EvenementBean>listerEvenementbymembre(@PathVariable(name="id") Long idpart)
	{
		return iMemberService.findEvenementparParticipant(idpart);		
	}
	
	
	@GetMapping("/publications")
	public CollectionModel<PublicationBean>listerpublication()
	{
		return publicationproxy.listeDesPublications();
		
	}
	@GetMapping("/publications/{id}")
	public EntityModel<PublicationBean> listerunepublication(@PathVariable Long id)
	{
		return publicationproxy.recupererUnePublication(id);
		
	}
	@GetMapping("/publications/auteur/{id}")
	public List<PublicationBean>listerpublicationbymembre(@PathVariable(name="id") Long idaut)
	{
		return iMemberService.findPublicationparauteur(idaut);		
	}
	@GetMapping("/outils/developpeur/{id}")
	public List<OutilBean>listeroutilbymembre(@PathVariable(name="id") Long iddev)
	{
		return iMemberService.findOutilpardev(iddev);		
	}
	
	
	@GetMapping("/fullmember/{id}")
	public Membre findAFullMember(@PathVariable(name="id") Long id)
	{
		Membre mbr=iMemberService.findMember(id);
		mbr.setPubs(iMemberService.findPublicationparauteur(id));
		mbr.setOutils(iMemberService.findOutilpardev(id));
		mbr.setEvs(iMemberService.findEvenementparParticipant(id));
		return mbr;		
	}
	@GetMapping(value = "/etudiants")
	public List<Etudiant> findAllEtudiants()
	{
		return etudiantRepository.getEtudiants();
	}
	@GetMapping("/students/encadrant/{id}")
	public List<Etudiant>listeretudiantsbyenseignant(@PathVariable(name="id") Long idEns)
	{
		return iMemberService.findStudentsByEnseignant(idEns);		
	}

	 @GetMapping("/files/{filename}")
	  @ResponseBody
	  public ResponseEntity<Resource> getFile(@PathVariable String filename) {
	    Resource file = iMemberService.load(filename);
	    return ResponseEntity.ok()
	        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
	  }

	
	@GetMapping(value="/membres/publication")
	public void createPubByuser(@RequestParam ("idmem") Long idmem , @RequestParam("idpub") Long idpub )
	{
		iMemberService.affecterauteurTopublication(idmem, idpub);
	}
	
	@PostMapping(value="/membres/evenement")
	public void createEvtByuser(@RequestParam ("idmem") Long idmem , @RequestParam("idevt") Long idevt )
	{
		iMemberService.affecterparticipantToevenement(idmem, idevt);
	}
	 
	@DeleteMapping(value="/evenement/{idevt}")
	public void deleteEvtMembre(@PathVariable Long idevt)
	{
		 iMemberService.deleteEvtMember(idevt);
	}
	
	@DeleteMapping(value="/publication/{idpub}")
	public void deletePubMembre(@PathVariable Long idpub)
	{
		 iMemberService.deletePubMember(idpub);
	}
	@PutMapping(value="/encadrant/student")
	public List<Etudiant> AnnulerAffectation(@RequestParam Long id , @RequestParam Long idens){
		
	return iMemberService.AnnulerAffectationEnsToEdu(id, idens);}
}

