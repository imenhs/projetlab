package com.example.demo.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.entities.Membre_Ev_Ids;
import com.example.demo.entities.Membre_Evenement;


public interface MembreEveRepository extends JpaRepository<Membre_Evenement, Membre_Ev_Ids> {
	@Query("select m from Membre_Evenement m where participant_id=:x")
	List<Membre_Evenement> findEveId(@Param ("x") Long ParticiapantId);
	
	@Transactional 
	@Modifying
	@Query("delete from Membre_Evenement m where id.evenement_id=:x")
	void deleteEvtMember(@Param ("x")long idevt);
}
