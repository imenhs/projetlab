package com.example.demo.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.entities.Membre_Outil;
import com.example.demo.entities.Membre_Outil_Ids;

public interface MembreOutilRepository extends JpaRepository<Membre_Outil, Membre_Outil_Ids> {
	@Query("select m from Membre_Outil m where developpeur_id=:x")
	List<Membre_Outil> findOutilId(@Param ("x") Long devId);


	@Transactional 
	@Modifying
	@Query("delete from Membre_Outil m where id.outil_id=:x")
	void deleteByAssociationId( @Param ("x")long idout);

}
