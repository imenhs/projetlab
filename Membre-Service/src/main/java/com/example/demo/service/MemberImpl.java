
package com.example.demo.service;

import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;

import com.example.demo.EvenementBean;
import com.example.demo.OutilBean;
import com.example.demo.PublicationBean;
import com.example.demo.dao.EnseignantChercheurRepository;
import com.example.demo.dao.EtudiantRepository;
import com.example.demo.dao.MemberRepository;
import com.example.demo.dao.MembreEveRepository;
import com.example.demo.dao.MembreOutilRepository;
import com.example.demo.dao.Membrepubrepository;
import com.example.demo.entities.EnseignantChercheur;
import com.example.demo.entities.Etudiant;
import com.example.demo.entities.Membre;
import com.example.demo.entities.Membre_Ev_Ids;
import com.example.demo.entities.Membre_Evenement;
import com.example.demo.entities.Membre_Outil;
import com.example.demo.entities.Membre_Outil_Ids;
import com.example.demo.entities.Membre_Pub_Ids;
import com.example.demo.entities.Membre_Publication;
import com.example.demo.proxies.EvenementProxy;
import com.example.demo.proxies.OutilProxyService;
import com.example.demo.proxies.PublicationProxy;

@Service
public class MemberImpl implements IMemberService {

	@Autowired
	MemberRepository memberRepository;
	@Autowired
	EtudiantRepository etudiantRepository;
	@Autowired
	EnseignantChercheurRepository enseignantChercheurRepository;
	@Autowired
	Membrepubrepository membrepubrepository;
	@Autowired
	PublicationProxy proxy;
	@Autowired
	EvenementProxy proxyEv;
	@Autowired
	OutilProxyService proxyOutil;

	@Autowired
	MembreEveRepository membreEverepository;
	@Autowired
	MembreOutilRepository membreOutilrepository;
	
	String curDir = System.getProperty("user.dir");
   
    private final Path root = Paths.get( curDir+"/src/main/webapp/CV/");
	public Membre addMember(Membre m) {
		
		memberRepository.save(m);
		return m;
	}

	public void deleteMember(Long id) {

		memberRepository.deleteById(id);

	}

	public Membre updateMember(Membre m) {

		return memberRepository.saveAndFlush(m);
	}

	public Membre findMember(Long id) {
		Membre m = (Membre) memberRepository.findById(id).get();
		return m;
	}

	public List<Membre> findAll() {

		return memberRepository.findAll();
	}

	public Membre findByCin(String cin) {
		return memberRepository.findByCin(cin);
	}

	public Membre findByEmail(String email) {
		return memberRepository.findByEmail(email);
	}

	public List<Membre> findByNom(String nom) {
		return memberRepository.findByNom(nom);
	}

	public List<Etudiant> findByDiplome(String diplome) {
		return etudiantRepository.findByDiplome(diplome);
	}

	public List<EnseignantChercheur> findByGrade(String grade) {

		return enseignantChercheurRepository.findByGrade(grade);
	}

	public List<EnseignantChercheur> findByEtablissement(String etablissement) {

		return enseignantChercheurRepository.findByEtablissement(etablissement);
	}

	public List<Etudiant> findAllEtudiants() {
		return etudiantRepository.findAll();
	}

	public List<EnseignantChercheur> findAllEnseignants() {
		return enseignantChercheurRepository.findAll();
	}

	@Override
	public Etudiant affecterencadrantToetudiant(Long idetd, Long idens) {
		
		Etudiant etd = etudiantRepository.findById(idetd).get();
		EnseignantChercheur ens = enseignantChercheurRepository.findById(idens).orElse(null);
		etd.setEncadrant(ens);

		return etudiantRepository.save(etd);
	}

	@Override
	public void affecterauteurTopublication(Long idauteur, Long idpub) {
		Membre mbr = memberRepository.findById(idauteur).get();
		Membre_Publication mbs = new Membre_Publication();
		mbs.setAuteur(mbr);
		mbs.setId(new Membre_Pub_Ids(idpub, idauteur));
		membrepubrepository.save(mbs);
	}

	@Override
	public List<PublicationBean> findPublicationparauteur(Long idauteur) {
		List<PublicationBean> pubs = new ArrayList<PublicationBean>();

		List<Membre_Publication> idpubs = membrepubrepository.findpubId(idauteur);
		if (!idpubs.isEmpty()) {
		idpubs.forEach(s -> {
			System.out.println(s);
			if ((s.getId().getPublication_id() !=null) && (proxy.recupererUnePublication(s.getId().getPublication_id())!=null) ) {
			pubs.add(proxy.recupererUnePublication(s.getId().getPublication_id()).getContent());}

		});}

		return pubs;
	}

	@Override
	public void affecterparticipantToevenement(Long idparticipant, Long idEv) {
		Membre mbr = memberRepository.findById(idparticipant).get();
		Membre_Evenement mev = new Membre_Evenement();
		mev.setParticipant(mbr);
		mev.setId(new Membre_Ev_Ids(idEv, idparticipant));
		membreEverepository.save(mev);
	}

	@Override
	public List<EvenementBean> findEvenementparParticipant(Long idparticipant) {
		List<EvenementBean> evs = new ArrayList<EvenementBean>();

		List<Membre_Evenement> idEvs = membreEverepository.findEveId(idparticipant);
		if (!idEvs.isEmpty()) {
		idEvs.forEach(v -> {
			System.out.println(v);
			if ((v.getId().getEvenement_id() !=null) && (proxyEv.recupererUnEvenement(v.getId().getEvenement_id())!=null) ){
			evs.add(proxyEv.recupererUnEvenement(v.getId().getEvenement_id()).getContent());}

		});}

		return evs;

	}

	@Override
	public void affecterDevelopToOutil(Long iddev, Long idoutil) {
		Membre mbr = memberRepository.findById(iddev).get();
		Membre_Outil mbs = new Membre_Outil();
		mbs.setDeveloppeur(mbr);
		mbs.setId(new Membre_Outil_Ids(idoutil, iddev));
		membreOutilrepository.save(mbs);
	}

	@Override
	public List<OutilBean> findOutilpardev(Long iddev) {
		List<OutilBean> outil = new ArrayList<OutilBean>();
		List<Membre_Outil> idoutil = membreOutilrepository.findOutilId(iddev);
		if (!idoutil.isEmpty()) {
		idoutil.forEach(s -> {
			System.out.println(s);
			if ((s.getId().getOutil_id() !=null) && (proxyOutil.recupererUnOutil(s.getId().getOutil_id())!=null) ){
			outil.add(proxyOutil.recupererUnOutil(s.getId().getOutil_id()).getContent());}
		});}
		return outil;
	}
@Override
	public List<Etudiant> findStudentsByEnseignant(Long idEns) {
		List<Etudiant> etudiantsEns = new ArrayList<Etudiant>();
		List<Etudiant> etudiants= etudiantRepository.getEtudiants();
		if (!etudiants.isEmpty()) { 	
		etudiants.forEach(s -> {
			if ((s.getEncadrant() != null) && (s.getEncadrant().getId()==idEns)) {etudiantsEns.add(s);}
			
		});}
		return etudiantsEns;
	}

	@Override
	public Resource load(String filename) {
		 try { System.out.println(root);
		      Path file = root.resolve(filename);
		      Resource resource = new UrlResource(file.toUri());
		  	   if (resource.exists() || resource.isReadable()) {
		        return resource;
		      } else {
		        throw new RuntimeException("Could not read the file!");
		      }
		    } catch (MalformedURLException e) {
		      throw new RuntimeException("Error: " + e.getMessage());
		    } 
	}


	public void deleteOutilMember(Long idout) {
		membreOutilrepository.deleteByAssociationId(idout);
	}

	@Override
	public void deleteEvtMember(Long idevt) {
		membreEverepository.deleteEvtMember(idevt);
		
	}

	@Override
	public void deletePubMember(Long idpub) {
		membrepubrepository.deletePubMember(idpub);
		
	}

	

	@Override
	public List<Etudiant> AnnulerAffectationEnsToEdu(Long id, Long idens) {
	    List<Etudiant> etudiants= findStudentsByEnseignant(idens);
		etudiants.forEach(e -> {
			 {e.setEncadrant(null);
			 memberRepository.saveAndFlush(e);}
				 });
		return etudiants;
	}
	
	@Override
	public void removeEnseignant(Long id) {
Membre mbr=memberRepository.findById(id).get();
		
		if (mbr.getClass()==EnseignantChercheur.class) {
		    AnnulerAffectationEnsToEdu(id, mbr.getId());
                
			enseignantChercheurRepository.deleteById(id);}
			
			//else if (e.getEncadrant().getId()!=id && e.getEncadrant().getId()!=null){etudiantRepository.deleteById(id);}
			 
		else memberRepository.deleteById(id);
	}

}
