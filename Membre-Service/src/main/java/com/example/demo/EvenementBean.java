package com.example.demo;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;
import lombok.NonNull;

@Data
public class EvenementBean {
	private Long id;
    private String titre;
    private Date date;
    private String lieu;

}

